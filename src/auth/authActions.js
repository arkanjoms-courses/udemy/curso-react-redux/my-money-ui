import {toastr} from 'react-redux-toastr'
import axios from 'axios'
import consts from '../consts'

export function login(values) {
    return submit(values, `${consts.OAPI_URL}/login`)
}

export function signup(values) {
    return submit(values, `${consts.OAPI_URL}/signup`)
}

async function submit(values, url) {
    try {
        const {data} = await axios.post(url, values)
        console.log(data)
        return {type: 'USER_FETCHED', payload: data}
    } catch (e) {
        e.response.data.errors.forEach(error => toastr.error('Erro', error))
    }
}

export function logout() {
    return {type: 'TOKEN_VALIDATED', payload: false}
}

export async function validateToken(token) {
    if (token) {
        try {
            const {data} = await axios.post(`${consts.OAPI_URL}/validateToken`, {token})
            return {type: 'TOKEN_VALIDATED', payload: data.valid}
        } catch (e) {
            return {type: 'TOKEN_VALIDATED', payload: false}
        }
    } else {
        return {type: 'TOKEN_VALIDATED', payload: false}
    }
}
