export const selectTab = (payload) => {
    return {type: 'TAB_SELECTED', payload}
}

export const showTabs = (...tabIds) => {
    const payload = {}
    tabIds.forEach(e => payload[e] = true)
    return {type: 'TAB_SHOWED', payload}
}
