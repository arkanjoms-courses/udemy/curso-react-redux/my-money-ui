import React from 'react'

import MenuItem from './menuItem'
import MenuTree from './menuTree'

export default () => (
    <ul className={'sidebar-menu'}>
        <MenuItem icon={'dashboard'} label={'Dashboard'} path={'/'}/>
        <MenuTree label={'Cadastro'} icon={'edit'}>
            <MenuItem path={'billingCycles'} label={'Ciclos de Pagamento'} icon={'usd'}/>
        </MenuTree>
    </ul>
)
