import React from 'react'

export default props => (
    <footer className={'main-footer'}>
        <strong>Copyright &copy; 2018</strong>
        <a href={'http://rafaelramos.eti.br'} target={'_blank'}> Rafael Ramos</a>.
    </footer>
)
