import axios from 'axios'
import {toastr} from 'react-redux-toastr'
import {initialize, stopSubmit} from 'redux-form'
import {selectTab, showTabs} from "../common/tab/tabActions"

const BASE_URL = 'http://localhost:3003/api/billingCycles'

export const getList = async () => {
    const {data} = await axios.get(BASE_URL)
    return {type: 'BILLING_CYCLES_FETCHED', payload: data}
}

export const create = async (values) => {
    try {
        await axios.post(BASE_URL, values)
        toastr.success('Sucesso', 'Operação realizada com sucesso!')
        return init()
    } catch (err) {
        err.response.data.errors.forEach(error => toastr.error('Erro', error))
        return [stopSubmit('billingCycleForm')]
    }
}

export const showUpdate = (billingCycle) => {
    return [showTabs('tabUpdate'), selectTab('tabUpdate'), initialize('billingCycleForm', billingCycle)]
}

export const showDelete = (billingCycle) => {
    return [showTabs('tabDelete'), selectTab('tabDelete'), initialize('billingCycleForm', billingCycle)]
}

export const update = async (values) => {
    try {
        await axios.put(`${BASE_URL}/${values._id}`, values)
        toastr.success('Sucesso', 'Operação realizada com sucesso!')
        return init()
    } catch (err) {
        err.response.data.errors.forEach(error => toastr.error('Erro', error))
        return [stopSubmit('billingCycleForm')]
    }
}

export const remove = async (values) => {
    try {
        await axios.delete(`${BASE_URL}/${values._id}`)
        toastr.success('Sucesso', 'Operação realizada com sucesso!')
        return init()
    } catch (err) {
        err.response.data.errors.forEach(error => toastr.error('Erro', error))
        return [stopSubmit('billingCycleForm')]
    }
}

export const init = () => {
    return [
        showTabs('tabList', 'tabCreate'),
        selectTab('tabList'),
        getList(),
        initialize('billingCycleForm', {credits: [{}], debts: [{}]})
    ]
}
