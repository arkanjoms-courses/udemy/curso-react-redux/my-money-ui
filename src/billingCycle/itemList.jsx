import React from 'react'
import {arrayInsert, arrayRemove, Field} from 'redux-form'

import Grid from '../common/layout/grid'
import Input from '../common/form/input'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'
import If from '../common/operator/if'

class ItemList extends React.Component {

    render() {
        return (
            <Grid cols={this.props.cols}>
                <fieldset>
                    <legend>{this.props.legend}</legend>

                    <table className={'table'}>
                        <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Valor</th>
                            <If test={this.props.showStatus}>
                                <th>Status</th>
                            </If>
                            <th className={'table-actions'}>Ações</th>
                        </tr>
                        </thead>
                        <tbody>
                        {this.renderRows()}
                        </tbody>
                    </table>
                </fieldset>
            </Grid>
        )
    }

    renderRows() {
        const {readOnly, field, list = []} = this.props
        return list.map((it, index) => (
            <tr key={index}>
                <td>
                    <Field name={`${field}[${index}].name`} component={Input}
                           placeholder={'Informe o nome'} readOnly={readOnly}/>
                </td>
                <td>
                    <Field name={`${field}[${index}].value`} component={Input}
                           placeholder={'Informe o valor'} readOnly={readOnly}/>
                </td>
                <If test={this.props.showStatus}>
                    <td>
                        <Field name={`${field}[${index}].status`} component={Input}
                               placeholder={'Informe o status'} readOnly={readOnly}/>
                    </td>
                </If>
                <td>
                    <button type={'button'} className={'btn btn-success'} onClick={() => this.add(index + 1)}><i
                        className={'fa fa-plus'}/></button>
                    <button type={'button'} className={'btn btn-warning'} onClick={() => this.add(index + 1, it)}><i
                        className={'fa fa-clone'}/></button>
                    <button type={'button'} className={'btn btn-danger'} onClick={() => this.remove(index)}><i
                        className={'fa fa-trash-o'}/></button>
                </td>
            </tr>
        ))
    }

    add(index, item = {}) {
        if (!this.props.readOnly) {
            this.props.arrayInsert('billingCycleForm', this.props.field, index, item)
        }
    }

    remove(index) {
        if (!this.props.readOnly && this.props.list.length > 1) {
            this.props.arrayRemove('billingCycleForm', this.props.field, index)
        }
    }
}

const mapDispatchToProps = dispatch => bindActionCreators({arrayInsert, arrayRemove}, dispatch)

export default connect(null, mapDispatchToProps)(ItemList)
