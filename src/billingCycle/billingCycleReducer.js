const INITIAL_STATE = {list: [], billingCycleEditing: {}}

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'BILLING_CYCLES_FETCHED':
            return {...state, list: action.payload}
        default:
            return state
    }
}
