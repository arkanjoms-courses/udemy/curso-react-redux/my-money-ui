import React from 'react'
import {bindActionCreators} from 'redux'
import {connect} from 'react-redux'

import {getList, showDelete, showUpdate} from "./billingCycleActions"

class BillingCycleList extends React.Component {
    componentWillMount() {
        this.props.getList()
    }

    render() {
        return (
            <div>
                <table className={'table'}>
                    <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Mês</th>
                        <th>Ano</th>
                        <th className={'table-actions'}>Ações</th>
                    </tr>
                    </thead>
                    <tbody>
                    {this.renderRows()}
                    </tbody>
                </table>
            </div>
        )
    }

    renderRows() {
        const list = this.props.list || []
        return list.map(it => (
            <tr key={it._id}>
                <td>{it.name}</td>
                <td>{it.month}</td>
                <td>{it.year}</td>
                <td>
                    <button className={'btn btn-warning'} onClick={() => this.props.showUpdate(it)}>
                        <i className={'fa fa-pencil'}/>
                    </button>
                    <button className={'btn btn-danger'} onClick={() => this.props.showDelete(it)}>
                        <i className={'fa fa-trash-o'}/>
                    </button>
                </td>
            </tr>
        ))
    }
}

const mapStateToProps = state => ({list: state.billingCycle.list})
const mapDispatchToProps = dispatch => (bindActionCreators({getList, showUpdate, showDelete}, dispatch))

export default connect(mapStateToProps, mapDispatchToProps)(BillingCycleList)
